module pokemon


import rand


import mongo_helper { ObjectId }


const (
	database_name = "pokeguesser"
	pokemon_collection_name = "pokemon"
)


struct PokedexEntry {
	description string [required]
	game string [required]
	language string [required]
}


struct PokedexNumber {
	number int [required]
	pokedex string [required]
}


pub struct Pokemon {
	id ObjectId [json: _id; required]
	number int [required]
	english_name string [required]
	names []string [required]
	pokedex_entries []PokedexEntry [required]
	pokedex_numbers []PokedexNumber [required]
	types []string [required]
	generation string [required]
	is_baby bool [required]
	is_legendary bool [required]
	is_mythical bool [required]
	habitat string [required]
	colour string [required]
	shape string [required]
	weight int [required]
	height int [required]
}


pub fn get_random_pokemon(client &C.mongoc_client_t) ?Pokemon {
	collection := client.get_structured_collection[Pokemon](database_name, pokemon_collection_name)
	total_pokemon_count := collection.count({})
	poke_index := rand.int_in_range(1, int(total_pokemon_count + 1)) or { 1 }
	cursor := collection.find({"number": poke_index})

	return cursor.next()
}
