use std::{collections::HashMap};

use async_std;
use mongodb::{Client, Collection, options::{ClientOptions, FindOneAndReplaceOptions}, bson::doc};
use serde::{Serialize, Deserialize};
use serde_json::Value;
use surf;


const POKEAPI_BASE_URL: &str = "https://pokeapi.co";
const POKEAPI_BASE_ENDPOINT: &str = "/api/v2";
const POKEAPI_SPECIES_ENDPOINT: &str = "/pokemon-species";
const POKEAPI_POKEMON_ENDPOINT: &str = "/pokemon";
const POKEMON_COUNT: i32 = 1010;


#[derive(Serialize, Deserialize, Debug)]
struct PokedexEntry {
    description: String,
    game: String,
    language: String
}


#[derive(Serialize, Deserialize, Debug)]
struct PokedexNumber {
    number: u64,
    pokedex: String
}


#[derive(Serialize, Deserialize, Debug)]
struct Pokemon {
    number: u64,
    english_name: Value,
    names: Vec<String>,
    pokedex_entries: Vec<PokedexEntry>,
    pokedex_numbers: Vec<PokedexNumber>,
    types: Vec<String>,
    generation: String,
    is_baby: bool,
    is_legendary: bool,
    is_mythical: bool,
    habitat: Value,
    colour: Value,
    shape: Value,
    weight: u64,
    height: u64
}


fn get_mongo_collection(options: ClientOptions, collection_name: &str) -> Collection<Pokemon> {
    Client::with_options(options).unwrap().database("pokeguesser").collection::<Pokemon>(collection_name)
}


fn get_english_name(names: &Vec<Value>) -> Value {
    for i in names.iter() {
        if i["language"]["name"] == "en" {
            return i["name"].clone()
        }
    }
    names[0]["name"].clone()
}


fn get_pokemon_from_responses(
    species_response: HashMap<String, serde_json::Value>,
    pokemon_response: HashMap<String, serde_json::Value>
) -> Pokemon {
    Pokemon {
        number: pokemon_response["id"].as_u64().unwrap(),
        english_name: get_english_name(species_response["names"].as_array().unwrap()),
        names: species_response["names"].as_array().unwrap().iter().map(
            |n| n["name"].as_str().unwrap().to_string()
        ).collect(),
        pokedex_entries: species_response["flavor_text_entries"].as_array().unwrap().iter().map(
            |e| PokedexEntry {
                description: e["flavor_text"].as_str().unwrap().to_string(),
                game: e["version"]["name"].as_str().unwrap().to_string(),
                language: e["language"]["name"].as_str().unwrap().to_string()
            }
        ).collect(),
        pokedex_numbers: species_response["pokedex_numbers"].as_array().unwrap().iter().map(
            |n| PokedexNumber{
                number: n["entry_number"].as_u64().unwrap(),
                pokedex: n["pokedex"]["name"].as_str().unwrap().to_string()
            }
        ).collect(),
        types: pokemon_response["types"].as_array().unwrap().iter().map(
            |t| t["type"]["name"].as_str().unwrap().to_string()
        ).collect(),
        generation: species_response["generation"]["name"].as_str().unwrap().to_string(),
        is_baby: species_response["is_baby"].as_bool().unwrap(),
        is_legendary: species_response["is_legendary"].as_bool().unwrap(),
        is_mythical: species_response["is_mythical"].as_bool().unwrap(),
        habitat: (|| {
            if species_response["habitat"].is_null() {
                Value::Null
            } else {
                species_response["habitat"]["name"].clone()
            }
        })(),
        colour: (|| {
            if species_response["color"].is_null() {
                Value::Null
            } else {
                species_response["color"]["name"].clone()
            }
        })(),
        shape: (|| {
            if species_response["shape"].is_null() {
                Value::Null
            } else {
                species_response["shape"]["name"].clone()
            }
        })(),
        weight: pokemon_response["weight"].as_u64().unwrap(),
        height: pokemon_response["height"].as_u64().unwrap()
    }
}


#[async_std::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let client: surf::Client = surf::Config::new()
        .set_base_url(surf::Url::parse(POKEAPI_BASE_URL)?)
        .try_into()?;

    let options = ClientOptions::parse("mongodb://root:password@localhost").await?;
    let collection = get_mongo_collection(options, "pokemon");

    for i in 1..POKEMON_COUNT + 1 {
        let species_response: HashMap<String, serde_json::Value> = client.get(
            format!("{}{}/{}", POKEAPI_BASE_ENDPOINT, POKEAPI_SPECIES_ENDPOINT, i)
        ).recv_json().await?;
        let pokemon_response: HashMap<String, serde_json::Value> = client.get(
            format!("{}{}/{}", POKEAPI_BASE_ENDPOINT, POKEAPI_POKEMON_ENDPOINT, i)
        ).recv_json().await?;
        let pokemon = get_pokemon_from_responses(species_response, pokemon_response);
        collection.find_one_and_replace(
            doc! {"number": i},
            pokemon,
            FindOneAndReplaceOptions::builder().upsert(true).build()
        ).await?;
    }
    Ok(())
}
