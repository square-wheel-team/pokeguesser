module main


import os
import vweb

import mongo

import pokemon { get_random_pokemon }


const default_mongo_uri = "mongodb://localhost:27017"


struct App {
	vweb.Context
}


fn (mut app App) hello() vweb.Result {
	return app.text('Hello')
}


['/foo']
fn (mut app App) world() vweb.Result {
	return app.text('World')
}


['/hello/:user']
fn (mut app App) hello_user(user string) vweb.Result {
	return app.text('Hello $user')
}


['/user'; get]
pub fn (mut app App) controller_get_user_by_id() vweb.Result {
	return app.text(app.query.str())
}


['/product/create'; post]
fn (mut app App) create_product() vweb.Result {
	return app.text('product')
}


['/pokemon']
fn (mut app App) get_pokemon() ?vweb.Result {
	mongo_uri := os.getenv_opt("MONGO_URI") or { default_mongo_uri }
	client := mongo.new_client(mongo_uri)
	if pokemon := get_random_pokemon(client) {
		return app.json(pokemon)
	} else {
		app.set_content_type("application/json")
		app.set_status(502, "Bad Gateway")
		return app.json({"error": "Couldn't get a random pokemon."})
	}
}


fn main() {
	vweb.run(&App{}, 8080)
}
