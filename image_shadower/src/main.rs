use std::fs;

use image::io::Reader as ImageReader;
use image::imageops::colorops::brighten;


const IMAGES_FOLDER: &str = "../images";
const SHADOWED_IMAGES_FOLDER: &str = "../shadowed_images";
const LIGHTENED_IMAGES_FOLDER: &str = "../lightened_images";


fn edit_images_for_filename(filename: &str, shadow: bool, lighten: bool) -> Result<String, Box<dyn std::error::Error>> {
    let image = ImageReader::open(format!("{}/{}", IMAGES_FOLDER, filename))?.decode()?;
    if shadow {
        brighten(&image, -255).save(format!("{}/{}", SHADOWED_IMAGES_FOLDER, filename))?;
    }
    if lighten {
        brighten(&image, 255).save(format!("{}/{}", LIGHTENED_IMAGES_FOLDER, filename))?;
    }
    Ok(format!("File {} done!", filename))
}


fn main() -> Result<(), Box<dyn std::error::Error>> {
    for (i, file) in fs::read_dir(IMAGES_FOLDER)?.enumerate() {
        let file = file?;
        println!("{}, {}", i, file.path().display());
        match edit_images_for_filename(file.file_name().to_str().unwrap(), true, true) {
            Ok(message) => println!("{}", message),
            Err(error) => println!("There was an error editing {}: {:?}", file.path().display(), error)
        }
    }
    Ok(())
}
