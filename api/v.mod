Module {
	name: 'api'
	description: 'PokeGuesser API. Returns a random Pokemon for you to guess'
	version: '0.0.1'
	license: 'MIT'
	dependencies: [
		"mongo"
	]
}
