module mongo_helper

import json
import mongo
import x.json2


pub struct ObjectId {
	id string [json: "\$oid"; required]
}


fn (object_id ObjectId) str() string {
	return object_id.id
}


struct MongoStructuredCollection[T] {
	collection &C.mongoc_collection_t
}


pub fn (collection MongoStructuredCollection[T]) find(query map[string]json2.Any) MongoIterator[T] {
	return MongoIterator[T]{ collection.collection.find(query) }
}


pub fn (collection MongoStructuredCollection[T]) find_with_opts(filter &C.bson_t, opts &C.bson_t) MongoIterator[T] {
	return MongoIterator[T]{ collection.collection.find_with_opts(filter, opts) }
}


pub fn (collection MongoStructuredCollection[T]) count(filter map[string]json2.Any) i64 {
	return collection.collection.count(filter)
}


pub fn (client &C.mongoc_client_t) get_structured_collection[T](db_name string, collection_name string) MongoStructuredCollection[T] {
	return MongoStructuredCollection[T] { client.get_collection(db_name, collection_name) }
}


struct MongoIterator[T] {
	cursor &C.mongoc_cursor_t
}


pub fn (iter MongoIterator[T]) next() ?T {
	document := mongo.new_bson()
	if iter.cursor.next_doc(&document) {
		return json.decode(T, document.str()) or { return none }
	}
	return none
}
